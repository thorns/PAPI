#! /bin/sh

################################################################################
# Prepare
################################################################################

# Set up shell
set -x                          # Output commands
set -e                          # Abort on errors



################################################################################
# Check options
################################################################################

if [ -z "$PAPI_DIR" ]; then
    echo "BEGIN ERROR"
    echo "PAPI selected but no PAPI_DIR set."
    echo "END ERROR"
fi



################################################################################
# Configure Cactus
################################################################################

# Set options
PAPI_INC_DIRS="${PAPI_DIR}/include"
PAPI_LIB_DIRS="${PAPI_DIR}/lib"
PAPI_LIBS='papi pfm'

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_PAPI     = 1"
echo "PAPI_DIR      = ${PAPI_DIR}"
echo "PAPI_INC_DIRS = ${PAPI_INC_DIRS}"
echo "PAPI_LIB_DIRS = ${PAPI_LIB_DIRS}"
echo "PAPI_LIBS     = ${PAPI_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(PAPI_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(PAPI_LIB_DIRS)'
echo 'LIBRARY           $(PAPI_LIBS)'
